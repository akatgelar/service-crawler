import oauth2 as oauth
import json
import time
import requests
import datetime
from pymongo import MongoClient
from pprint import pprint

CONSUMER_KEY = "5rwCykqs4Cdqu2Q3glDBu1A5R"
CONSUMER_SECRET = "zVDelnTBmpxBvEoIj82ZmGhB36UZGo0H9XPtKc15IjG89qyNar"
ACCESS_KEY = "159491199-pQVpNqNvVl0Dj87YTmXHw1DlUkwCZTPbcXJUab4R"
ACCESS_SECRET = "im58AShCFO1uGHZSFlISrHaBIdWVx9bz8d4MRRsFKqQS5"

consumer = oauth.Consumer(key=CONSUMER_KEY, secret=CONSUMER_SECRET)
access_token = oauth.Token(key=ACCESS_KEY, secret=ACCESS_SECRET)

client = oauth.Client(consumer, access_token)

dbclient = MongoClient("mongodb://localhost:27017/")
db = dbclient.SentimentAnalysis

# def __init__(self, id):
#     self.last_id=id


def getTweet(query, count, result_type, max_id, search_by, search_value, is_next):

    method = "GET"
    headers = "{'Content-type': 'application/json'}"

    if(max_id != None):
        url = "https://api.twitter.com/1.1/search/tweets.json"+"?q="+query + \
            "&count="+count+"&result_type="+result_type+"&max_id="+str(max_id)
    else:
        url = "https://api.twitter.com/1.1/search/tweets.json" + \
            "?q="+query+"&count="+count+"&result_type="+result_type

    print(method)
    print(url)
    print(headers)
    # print(search_by)
    # print(search_value)

    try:
        response, data = client.request(url, method=method, headers=headers)

        # print(response)
        # print(str(data))

        tweets_data = json.loads(data)
        # print(tweets_data)

        # save to file
        # fh = open("json_result.txt", "w")
        # fh.write(json.dumps(tweets_data))
        # fh.close()

        i = 0
        max = len(tweets_data["statuses"])
        print("count data : " + str(max))
        if(max > 0):
            for tweet in tweets_data["statuses"]:
                if(i == 0):
                    i = i+1
                else:
                    tweet["mongo_created_at"] = datetime.datetime.now()
                    tweet["search_by"] = search_by
                    tweet["search_value"] = search_value
                    tweet["scoring_progress"] = False
                    tweet["scoring_progress_date"] = ""
                    tweet["scoring_point"] = ""
                    tweet["scoring_sentiment"] = ""
                    if "retweeted_status" in tweet:
                        print("(%i) %i %s @%s %s" % (i, len(
                            tweet["retweeted_status"]), tweet["id_str"], tweet["created_at"], tweet["text"][:10]))
                    else:
                        print("(%i) %i %s @%s %s" % (
                            i, 0, tweet["id_str"], tweet["created_at"], tweet["text"][:10]))
                        result = db.tweets_old.insert_one(tweet)
                    i = i+1
                    if(i == max-1):
                        max_id = tweet["id"]
                        is_next = True
                        getTweet(query, count, result_type, max_id,
                                 search_by, search_value, is_next)
    except Exception as e:
        print(e)


def getTopic():

    i = 0
    search_by = "hashtag"
    search_value = "2019GantiPresiden"

    print("================================")
    print("start topic")
    print("================================")
    print("topic index : " + str(i))
    print("search_by : " + search_by)
    print("search_value : " + search_value)
    print("================================")

    result_id = db.tweets_old.find(
        {"search_by": search_by, "search_value": search_value}).sort('_id', -1).limit(1)

    count = "100"
    result_type = "recent"
    max_id = None
    is_next = False

    if(search_by == "hashtag"):
        query = "%23"+search_value
    elif(search_by == "keyword"):
        query = search_value
    else:
        query = search_value

    if(result_id.count() > 0):
        last_id = result_id[0]['id_str']
        # print(result_id[0]['_id'])
        # print(result_id[0]['id_str'])
        max_id = last_id

    getTweet(query, count, result_type, max_id,
             search_by, search_value, is_next)


if __name__ == '__main__':
        getTopic()
